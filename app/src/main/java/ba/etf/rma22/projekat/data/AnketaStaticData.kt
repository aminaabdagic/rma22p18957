package ba.etf.rma22.projekat.data


import ba.etf.rma22.projekat.data.models.Anketa
import java.util.*

class Korisnik(){
    val ime: String="Neko";
    val prezime:String="Nekić";
    var grupa:MutableList<String> = mutableListOf("studenti_ekonomije");
    var upisanaIstrazivanja:MutableList<String> =mutableListOf("Fizička aktivnost")
    var proslagodina: String? =null;
}
 val k= Korisnik();
fun myankete(): List<Anketa> {
    return allAnkete().filter { a-> k.grupa.contains(a.nazivGrupe)&& k.upisanaIstrazivanja.contains((a.nazivIstrazivanja))};
}
fun allAnkete(): List<Anketa> {
    val c1: Calendar = Calendar.getInstance()
    val c2: Calendar = Calendar.getInstance()
    c1.set(2022,3,1)
    c2.set(2022,7,19)
    val a1: Anketa =
        Anketa("Anketa 1", "Fizička aktivnost",c1.time,c2.time, null,30,"studenti_ekonomije",0f)
    c1.set(2022,2,2)
    c2.set(2022,2,10)
    val a2: Anketa =
        Anketa("Anketa 1","Informatička pismenost",c1.time,c2.time,
            null,20,"studenti_prava",0f)
    c1.set(2022,8,2)
    c2.set(2022,8,25)
    val a3: Anketa = Anketa("Anketa 1","Nasilje",c1.time,c2.time,
        null,20,"studenti_pedagogije",0f)
    val c: Calendar= Calendar.getInstance()
    c.set(2022,2,28)
    c1.set(2022,2,22)
    c2.set(2022,8,10)
    val a5: Anketa = Anketa("Anketa 1","Apsolventska ekskurzija",c1.time,
        c2.time,c.time,5,"studenti_filozofije",0.5f)
    c1.set(2022,3,9)
    c2.set(2022,6,30)
    val a6: Anketa = Anketa("Anketa 1","Opće Znanje",c1.time,c2.time,null,40,"studenti_unsa",0f);

    return listOf(a1,
        a2,
        a3,
        a5,
        a6)}
fun doneAnkete(): List<Anketa> {
    return allAnkete().filter{ a->(a.datumRada!=null && k.grupa.contains(a.nazivGrupe))}
}

fun futureAnkete(): List<Anketa> {
    return myankete().filter{ a->(Date()<a.datumPocetak&& k.grupa.contains(a.nazivGrupe))}
}
fun notTakenAnkete(): List<Anketa> {
    return myankete().filter{ a->(Date()>(a.datumKraj)&& k.grupa.contains(a.nazivGrupe))};
}
package ba.etf.rma22.projekat


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.view.AnketaListAdapter
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {
    private lateinit var listaAnketa: RecyclerView;
    private lateinit var listaAnketaAdapter: AnketaListAdapter
    private lateinit var filterAnketa: Spinner
    private var anketaListViewModel = AnketaListViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val filteri: List<String> = listOf(
            "Sve moje ankete",
            "Sve ankete",
            "Urađene ankete",
            "Buduće ankete",
            "Prošle ankete"
        )


        val spinner = findViewById<Spinner>(R.id.filterAnketa)
        if (spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, filteri
            )
            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    if (filteri[position] == "Sve moje ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getMyAnkete())
                    } else if (filteri[position] == "Sve ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getAllAnkete())
                    } else if (filteri[position] == "Urađene ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getDoneAnkete())
                    } else if (filteri[position] == "Buduće ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getFuture())
                    } else {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getNotTakenAnkete())
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }

        val dodaj = findViewById<FloatingActionButton>(R.id.upisDugme)
        dodaj.setOnClickListener {
            val intent = Intent(this, UpisIstrazivanje::class.java)
            startActivity(intent)
        }
        listaAnketa = findViewById(R.id.listaAnketa)
        /*  listaAnketa.layoutManager = LinearLayoutManager(
          this,
          LinearLayoutManager.VERTICAL,
          false
      )*/
        listaAnketa.setLayoutManager(GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false))
        val dividerItemDecoration = DividerItemDecoration(
            listaAnketa.context,
            GridLayoutManager.VERTICAL
        )
        listaAnketa.addItemDecoration(dividerItemDecoration)
        listaAnketaAdapter = AnketaListAdapter(arrayListOf())
        listaAnketa.adapter = listaAnketaAdapter
        listaAnketaAdapter.updateAnkete(anketaListViewModel.getAllAnkete())
        /*   if(intent?.action == Intent.ACTION_SEND && intent?.type == "text/plain")
               handleSendText(intent)*/
    }
    override fun onResume() {
        super.onResume()
        val filteri: List<String> = listOf(
            "Sve moje ankete",
            "Sve ankete",
            "Urađene ankete",
            "Buduće ankete",
            "Prošle ankete"
        )


        val spinner = findViewById<Spinner>(R.id.filterAnketa)
        if (spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, filteri
            )
            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    if (filteri[position] == "Sve moje ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getMyAnkete())
                    } else if (filteri[position] == "Sve ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getAllAnkete())
                    } else if (filteri[position] == "Urađene ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getDoneAnkete())
                    } else if (filteri[position] == "Buduće ankete") {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getFuture())
                    } else {
                        listaAnketaAdapter.updateAnkete(anketaListViewModel.getNotTakenAnkete())
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }

        val dodaj = findViewById<FloatingActionButton>(R.id.upisDugme)
        dodaj.setOnClickListener {
            val intent = Intent(this, UpisIstrazivanje::class.java)
            startActivity(intent)
        }
        listaAnketa = findViewById(R.id.listaAnketa)
        /*  listaAnketa.layoutManager = LinearLayoutManager(
          this,
          LinearLayoutManager.VERTICAL,
          false
      )*/
        listaAnketa.setLayoutManager(GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false))
        val dividerItemDecoration = DividerItemDecoration(
            listaAnketa.context,
            GridLayoutManager.VERTICAL
        )
        listaAnketa.addItemDecoration(dividerItemDecoration)
        listaAnketaAdapter = AnketaListAdapter(arrayListOf())
        listaAnketa.adapter = listaAnketaAdapter
        listaAnketaAdapter.updateAnkete(anketaListViewModel.getAllAnkete())
    }

}
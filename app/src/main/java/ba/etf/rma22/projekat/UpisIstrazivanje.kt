package ba.etf.rma22.projekat


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import ba.etf.rma22.projekat.data.k
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.models.Istrazivanje

import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.getIstrazivanjeByGodina

class UpisIstrazivanje : AppCompatActivity() {
    private lateinit var odabirGodina: Spinner
    private lateinit var odabirIstrazivanja: Spinner
    private lateinit var odabirGrupa:Spinner
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upis_istrazivanje)
        odabirGodina=findViewById(R.id.odabirGodina)
        odabirIstrazivanja=findViewById(R.id.odabirIstrazivanja)
        odabirGrupa=findViewById(R.id.odabirGrupa)
        val upisiMe=findViewById<Button>(R.id.dodajIstrazivanjeDugme)
        upisiMe.isEnabled=false
        val godine:MutableList<String> = mutableListOf("","1","2","3","4","5")
        var ist:String="";
        var god:Int=0;
        var grup:String=""
        var ii: MutableList<String> = mutableListOf(" ");
        var i= listOf(Istrazivanje("",0))
        if (odabirGodina != null) {

            var adapter1 = ArrayAdapter(this, android.R.layout.simple_spinner_item, godine)
            val godine1:MutableList<String>
            if(k.proslagodina!=null) {
                godine1 = mutableListOf(k.proslagodina.toString())
                godine1.addAll(godine.filter { g -> g != k.proslagodina && g != "" } as MutableList<String>)
                adapter1 = ArrayAdapter(this, android.R.layout.simple_spinner_item, godine1)
            }
            odabirGodina.adapter = adapter1
            odabirGodina.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    val istrazivanja3 = parent.getItemAtPosition(position).toString()
                    var g = 0;
                    if (istrazivanja3 == "1") g = 1
                    else if (istrazivanja3 == "2") g = 2
                    else if (istrazivanja3 == "3") g = 3
                    else if (istrazivanja3 == "4") g = 4
                    else if (istrazivanja3 == "5") g = 5
                    else g = 0
                    i = getIstrazivanjeByGodina(g).filter { a-> !k.upisanaIstrazivanja.contains(a.naziv) }
                    var i2 = i.iterator()
                    ii = mutableListOf<String>("")
                    while (i2.hasNext()) {
                        ii.add(i2.next().naziv)
                    }
                    var adapter2 = ArrayAdapter<String>(
                        applicationContext,
                        android.R.layout.simple_spinner_item,
                        ii
                    )
                    odabirIstrazivanja.adapter = adapter2
                    odabirIstrazivanja.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent1: AdapterView<*>, view: View, position1: Int, id1: Long) {
                                var grupa = GrupaRepository.getGroupsByIstrazivanje(
                                    parent1.getItemAtPosition(position1).toString()
                                )
                                var gg: MutableList<String> = mutableListOf("")
                                var g1 = grupa.iterator()
                                while (g1.hasNext()) {
                                    gg.add(g1.next().naziv)
                                }
                                var adapter3 = ArrayAdapter<String>(
                                    applicationContext,
                                    android.R.layout.simple_spinner_item,
                                    gg
                                )
                                odabirGrupa.adapter = adapter3
                                odabirGrupa.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onItemSelected(
                                            parent2: AdapterView<*>,
                                            view: View, position2: Int, id1: Long
                                        ) {

                                            if (istrazivanja3 != "" && parent1.getItemAtPosition(position1)
                                                    .toString() != "" && parent2.getItemAtPosition(position2).toString() != ""
                                            ) {
                                                upisiMe.isEnabled = true
                                                ist=parent1.getItemAtPosition(position1).toString()
                                                god=g
                                                grup=parent2.getItemAtPosition(position2).toString()
                                            }
                                        }


                                        override fun onNothingSelected(p0: AdapterView<*>?) {
                                            TODO("Not yet implemented")
                                        }
                                    }
                            }

                            override fun onNothingSelected(p0: AdapterView<*>?) {
                                TODO("Not yet implemented")
                            }

                        }
                }
                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }}

        upisiMe.setOnClickListener {
            k.upisanaIstrazivanja.add(ist)
            k.grupa.add(grup)
            k.proslagodina= god.toString()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }}
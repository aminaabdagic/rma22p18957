package ba.etf.rma22.projekat.data


import ba.etf.rma22.projekat.data.models.Istrazivanje


public fun getall(): List<Istrazivanje> {
    return listOf(
        Istrazivanje("Fizička aktivnost",2), Istrazivanje("Informatička pismenost",1),
        Istrazivanje("Opće znanje",4), Istrazivanje("Nasilje",5), Istrazivanje("Apsolventska ekskurzija",3)
    )
}
public fun getistrazivanjegodina( godina:Int): List<Istrazivanje> {
    return getall().filter { n->n.godina==godina }
}
public fun upisani(): List<Istrazivanje> {
    return getall().filter { a-> k.upisanaIstrazivanja.contains(a.naziv) }
}
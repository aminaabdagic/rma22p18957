package ba.etf.rma22.projekat.viewmodel


import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository

class AnketaListViewModel {
    public fun getAllAnkete():List<Anketa>{
        return AnketaRepository.getAll();

    }
    public fun getMyAnkete():List<Anketa>{
        return AnketaRepository.getMyAnkete();

    }
    public fun getDoneAnkete(): List<Anketa>{
        return AnketaRepository.getDone()
    }
    public fun getNotTakenAnkete(): List<Anketa>{
        return AnketaRepository.getNotTaken()
    }
    public fun getFuture():List<Anketa>{
        return AnketaRepository.getFuture()
    }
}
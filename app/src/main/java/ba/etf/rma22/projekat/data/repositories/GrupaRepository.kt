package ba.etf.rma22.projekat.data.repositories


import ba.etf.rma22.projekat.data.grupebyistrazivanje
import ba.etf.rma22.projekat.data.models.Grupa

object GrupaRepository {
    fun getGroupsByIstrazivanje(nazivIstrazivanja:String) : List<Grupa>{
        return grupebyistrazivanje(nazivIstrazivanja);
    }

}
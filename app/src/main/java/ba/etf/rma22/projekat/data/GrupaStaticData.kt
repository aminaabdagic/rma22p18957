package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Grupa

fun allgrupe(): List<Grupa> {
    return listOf(
        Grupa("studenti_ekonomije","Fizička aktivnost"),
        Grupa("studenti_prava","Informatička pismenost"),
        Grupa("studenti_unsa","Opće znanje"),
        Grupa("Nasilje","studenti_pedagogije"), Grupa("Apsolventska ekskurzija","studenti_filozofije")
    )
}
public fun grupebyistrazivanje(nazivIstrazivanja: String): List<Grupa> {
    return allgrupe().filter { n-> n.nazivIstrazivanja == nazivIstrazivanja }
}
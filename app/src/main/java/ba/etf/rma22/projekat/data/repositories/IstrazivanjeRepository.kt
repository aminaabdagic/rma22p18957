package ba.etf.rma22.projekat.data.repositories


import ba.etf.rma22.projekat.data.getall
import ba.etf.rma22.projekat.data.getistrazivanjegodina
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.upisani

object IstrazivanjeRepository {
    public fun  getIstrazivanjeByGodina(godina:Int) : List<Istrazivanje>{
        return getistrazivanjegodina(godina)
    }


    fun getAll() : List<Istrazivanje>{
        return getall();
    }


    fun getUpisani() : List<Istrazivanje>{
        return upisani();
    }


}
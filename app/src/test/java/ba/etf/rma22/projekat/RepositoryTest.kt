package ba.etf.rma22.projekat

import ba.etf.rma22.projekat.data.k
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.myankete
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import junit.framework.Assert.assertEquals
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.hasItem
import org.hamcrest.Matchers.hasProperty
import org.hamcrest.CoreMatchers.`is` as Is
import org.junit.Test


class RepositoryTest {
    @Test
     fun testGetMyAnkete(){

        k.grupa.add("studenti_unsa")
        k.upisanaIstrazivanja.add("Opće Znanje")
        val done= AnketaRepository.getMyAnkete()
        println(done[0].nazivGrupe+done[1].nazivGrupe)
        assertEquals(done.size,2)

        assertThat(done, hasItems<Anketa>(hasProperty("nazivGrupe", Is("studenti_unsa"))))
        k.grupa.removeLast()
        k.upisanaIstrazivanja.removeLast()
     }
    @Test
    fun testGetAllAnkete(){
        val ankete=AnketaRepository.getAll()
        assertEquals(ankete.size,5)
        assertThat(ankete, hasItems<Anketa>(hasProperty("nazivIstrazivanja", Is ("Fizička aktivnost")),
            hasProperty("nazivIstrazivanja",Is("Informatička pismenost")),
        hasProperty("nazivIstrazivanja",Is("Nasilje")),
            hasProperty("nazivIstrazivanja",Is("Apsolventska ekskurzija")),
        hasProperty("nazivIstrazivanja",Is("Opće Znanje"))))
    }
    @Test
    fun testDoneAnkete(){
        k.grupa.add("studenti_filozofije")
        k.upisanaIstrazivanja.add("Apsolventska ekskurzija")
        val done= AnketaRepository.getDone()
        assertEquals(done.size,1)
        assertThat(done,hasItem<Anketa>(hasProperty("nazivGrupe", Is("studenti_filozofije"))))
        k.grupa.removeLast()
        k.upisanaIstrazivanja.removeLast()
    }
    @Test
    fun testFutureAnkete(){

        k.grupa.add("studenti_pedagogije")
        k.upisanaIstrazivanja.add("Nasilje")
        //println(k.grupa[1])
        val future=AnketaRepository.getFuture()
        assertEquals(1,future.size)
        assertThat(future, hasItem<Anketa>(hasProperty("nazivIstrazivanja", Is("Nasilje"))))
        k.grupa.removeLast()
        k.upisanaIstrazivanja.removeLast()
    }
@Test
fun testNotTakenAnkete(){
    val pom:MutableList<String> = k.grupa
    val pom1:MutableList<String> = k.upisanaIstrazivanja
    k.grupa.add("studenti_prava")
    k.upisanaIstrazivanja.add("Informatička pismenost")
    val nottaken=AnketaRepository.getNotTaken()
    assertEquals(nottaken.size,1)
    assertThat(nottaken, hasItems<Anketa>(hasProperty("nazivIstrazivanja", Is("Informatička pismenost")),
        hasProperty("naziv",Is("Anketa 1"))))
    k.grupa.removeLast()
    k.upisanaIstrazivanja.removeLast()
}
@Test
fun testGetGroupsByIstrazivanje(){
    val g=GrupaRepository.getGroupsByIstrazivanje("Informatička pismenost")
    assertThat(g, hasItem<Grupa>(hasProperty("nazivIstrazivanja", Is("Informatička pismenost"))))
}
    @Test
    fun testgetIstrazivanjeByGodina(){
        val g=IstrazivanjeRepository.getIstrazivanjeByGodina(1)
        assertThat(g, hasItem<Istrazivanje>(hasProperty("naziv", Is("Informatička pismenost"))))
    }

    @Test
    fun testGetAll(){
     val pom=IstrazivanjeRepository.getAll()
        assertThat(pom, hasItems<Istrazivanje>(hasProperty("naziv",Is("Fizička aktivnost")),
            hasProperty("naziv", Is("Informatička pismenost")),
            hasProperty("naziv", Is("Opće znanje")),
            hasProperty("naziv", Is("Nasilje")),
            hasProperty("naziv", Is("Apsolventska ekskurzija"))))
    }

    @Test
    fun testGetUpisani(){
        val pom=IstrazivanjeRepository.getUpisani()
        val pom1:MutableList<String> = mutableListOf()
        val it=pom.iterator()
        while(it.hasNext()){
            pom1.add(it.next().naziv)
        }
        assertEquals(k.upisanaIstrazivanja,pom1)
    }
}